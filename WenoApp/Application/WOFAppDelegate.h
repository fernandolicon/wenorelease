//
//  AppDelegate.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 11/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WOFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

