//
//  Settings.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 13/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#ifndef WenoApp_Settings_h
#define WenoApp_Settings_h

#define FACEBOOK_SCHEME @"fb865935076833756"

static NSString * const kClientId = @"748718519466-ssbb0fkn5o7uvd13n0oirqeteolsb7mj.apps.googleusercontent.com";
static NSString * const wenoWS = @"https://weno.herokuapp.com";
static NSString * const wenoAPI = @"https://weno.herokuapp.com/api/v1/";

#endif
