//
//  WOFRestaurant.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 23/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WOFObject.h"

@interface WOFRestaurant : WOFObject

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *wifiPassword;
@property (nonatomic, strong) NSString *payType;
@property (nonatomic, strong) NSString *wifiName;
@property (nonatomic, strong) NSString *codeDress;
@property (nonatomic, strong) NSString *zone;
@property (nonatomic, strong) NSString *typeMenu;
@property BOOL hasDelivery;
@property BOOL hasReservations;
@property BOOL eventInPlace;
@property BOOL eventInYourPlace;
@property NSInteger amountPreference;
@property double latitude;
@property double longitude;

- (instancetype) initWithDictionary: (NSDictionary *) restaurantDictionary;
- (instancetype) initWithAlternativeDictionary: (NSDictionary *) restaurantDictionary;

@end
