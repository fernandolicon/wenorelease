//
//  WOFFood.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 23/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WOFObject.h"

@interface WOFFood : WOFObject

@property (nonatomic, strong) NSString *typeCook;
@property NSInteger restaurantID;
@property (nonatomic, strong) NSString *restaurantName;
@property BOOL prefered;
@property float price;
@property (nonatomic, strong) NSString *timeCook;
@property (nonatomic, strong) NSString *diet;

- (instancetype) initWithDictionary: (NSDictionary *) foodDictionary;
- (instancetype) initForMenuWithDictionary: (NSDictionary *) foodDictionary forRestaurantName: (NSString *) restaurantName;

@end
