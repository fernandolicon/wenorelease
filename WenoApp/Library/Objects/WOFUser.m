//
//  WOFUser.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 9/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFUser.h"
#import <GoogleSignIn/GoogleSignIn.h>

@implementation WOFUser

- (id)initWithCoder:(NSCoder *)decoder{
    if (self = [super init]) {
        self.name = [decoder decodeObjectForKey:@"name"];
        NSString *auxString = [decoder decodeObjectForKey:@"age"];
        self.age = [auxString integerValue];
        self.birthday = [decoder decodeObjectForKey:@"birthday"];
        self.gender = [decoder decodeObjectForKey:@"gender"];
        self.sentimental = [decoder decodeObjectForKey:@"sentimental"];
        self.phone = [decoder decodeObjectForKey:@"phone"];
        self.city = [decoder decodeObjectForKey:@"city"];
        self.facebook = [decoder decodeObjectForKey:@"facebook"];
        self.photo = [decoder decodeObjectForKey:@"photo"];
        self.accessToken = [decoder decodeObjectForKey:@"token"];
        self.userId = [decoder decodeObjectForKey:@"userID"];
        self.userImage = [decoder decodeObjectForKey:@"userImage"];
        auxString = [decoder decodeObjectForKey:@"synched"];
        self.synched = ([auxString isEqualToString:@"YES"]) ? YES : NO;
    }
    
    return self;
}

- (id) initWithFacebookResponse: (NSDictionary *) facebookResponse{
    if (self = [super init]) {
        self.name = [facebookResponse objectForKey:@"name"];
        self.gender = [facebookResponse objectForKey:@"gender"];
        self.email = [facebookResponse objectForKey:@"email"];
        self.userId = [facebookResponse objectForKey:@"id"];
        NSURL *profilePictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=100&height=100", self.userId]];
        self.userImageURL = profilePictureURL;
        self.userImage = [NSData dataWithContentsOfURL:profilePictureURL];
        self.synched = NO;
    }
    
    return self;
}

- (id) initWithGoogleProfile: (GIDGoogleUser *) user {
    if (self = [super init]) {
        self.name = user.profile.name;
        self.userId = user.userID;
        self.userImageURL = [user.profile imageURLWithDimension:100];
        self.userImage = [NSData dataWithContentsOfURL:self.userImageURL];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.name forKey:@"name"];
    NSString *auxString = [NSString stringWithFormat:@"%li", (long)self.age];
    [encoder encodeObject:auxString forKey:@"age"];
    [encoder encodeObject:self.birthday forKey:@"birthday"];
    [encoder encodeObject:self.gender forKey:@"gender"];
    [encoder encodeObject:self.sentimental forKey:@"sentimental"];;
    [encoder encodeObject:self.phone forKey: @"phone"];
    [encoder encodeObject:self.city forKey:@"city"];
    [encoder encodeObject:self.facebook forKey:@"facebook"];
    [encoder encodeObject:self.photo forKey:@"photo"];
    [encoder encodeObject:self.accessToken forKey:@"token"];
    [encoder encodeObject:self.userImage forKey:@"userID"];
    [encoder encodeObject:self.userImage forKey:@"userImage"];
    [encoder encodeObject:@"NO" forKey:@"synched"];
}

@end
