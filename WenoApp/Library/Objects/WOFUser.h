//
//  WOFUser.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 9/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GIDGoogleUser;

@interface WOFUser : NSObject <NSCoding>

@property (strong, nonatomic) NSString *name;
@property NSInteger age;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *birthday;
@property (strong, nonatomic) NSString *gender;
@property (strong, nonatomic) NSString *sentimental;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *facebook;
@property (strong, nonatomic) NSString *photo;
@property (strong, nonatomic) NSString *accessToken;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSData *userImage;
@property (strong, nonatomic) NSURL *userImageURL;
@property BOOL synched;

- (id)initWithCoder:(NSCoder *)decoder;
- (id) initWithFacebookResponse: (NSDictionary *) facebookResponse;
- (id) initWithGoogleProfile: (GIDGoogleUser *) user;
-(void)encodeWithCoder:(NSCoder *)encoder;

//This property will remain unused until further implementations
//@property (strong, nonatomic) NSString *twitter;

@end
