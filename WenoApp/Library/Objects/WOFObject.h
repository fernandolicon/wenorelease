//
//  WOFObjects.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 26/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface WOFObject : RLMObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *objectDescription;
@property (nonatomic, strong) NSString *primaryPhoto;
@property (nonatomic, strong) NSString *bannerPhoto;
@property (nonatomic, strong) NSData *primaryPhotoData;
@property (nonatomic, strong) NSData *bannerPhotoData;
@property (nonatomic, strong) NSString *information;
@property BOOL isRestaurant;
@property NSInteger objectID;
@property NSString *uuid;

@end
