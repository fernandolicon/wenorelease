//
//  WOFRequestHelper.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 23/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFRequestHelper.h"

@implementation WOFRequestHelper

@synthesize delegate;

#pragma mark - Lists requests

- (void) listofAllPlacesPerformSelector: (NSString *) selector{
    NSString *url = [NSString stringWithFormat:@"%@restaurants/", wenoAPI];
    [WOFRequestHelper performRequestForUrl:url withSuccess:^(NSDictionary *response) {
        [delegate requestDidFinishRequestingData:response performSelector:selector];
    } withFailure:^(NSError *error) {
        [delegate requestErrorInConnection];
    }];
}

- (void) listofPlacesBy: (NSString *) userSelection forType: (NSString *) type performSelector: (NSString *) selector{
    NSString *url = [NSString stringWithFormat:@"%@restaurant-%@/%@", wenoAPI, type, userSelection];
    NSString *strUrl=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [WOFRequestHelper performRequestForUrl:strUrl withSuccess:^(NSDictionary *response) {
        [delegate requestDidFinishRequestingData:response performSelector:selector];
    } withFailure:^(NSError *error) {
        [delegate requestErrorInConnection];
    }];
}

- (void) restaurantType: (NSString *) userSelection performSelector: (NSString *) selector{
    NSString *url = [NSString stringWithFormat:@"%@%@", wenoAPI, userSelection];
    
    [WOFRequestHelper performRequestForUrl:url withSuccess:^(NSDictionary *response) {
        [delegate requestDidFinishRequestingData:response performSelector:selector];
    } withFailure:^(NSError *error) {
        [delegate requestErrorInConnection];
    }];
}

- (void) menuforRestaurantwithId: (NSInteger) restaurantID performSelector: (NSString *) selector{
    NSString *url = [NSString stringWithFormat:@"%@restaurants/%li", wenoAPI, (long)restaurantID];
    
    [WOFRequestHelper performRequestForUrl:url withSuccess:^(NSDictionary *response) {
        [delegate requestDidFinishRequestingData:response performSelector:selector];
    } withFailure:^(NSError *error) {
        [delegate requestErrorInConnection];
    }];
}

#pragma mark - Search requests

- (void) searchString: (NSString *) searchString performSelector: (NSString *) selector{
    NSString *url = [NSString stringWithFormat:@"%@search?search=%@", wenoAPI,searchString];
    
    [WOFRequestHelper performRequestForUrl:url withSuccess:^(NSDictionary *response) {
        [delegate requestDidFinishRequestingData:response performSelector:selector];
    } withFailure:^(NSError *error) {
        [delegate requestErrorInConnection];
    }];
}

- (void) searchRestaurantforCode: (NSString *) scannedCode performSelector: (NSString *) selector{
    NSString *url = [NSString stringWithFormat:@"%@search-code?code=%@", wenoAPI,scannedCode];
    NSString *strUrl=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [WOFRequestHelper performRequestForUrl:strUrl withSuccess:^(NSDictionary *response) {
        [delegate requestDidFinishRequestingData:response performSelector:selector];
    } withFailure:^(NSError *error) {
        [delegate requestErrorInConnection];
    }];
}

#pragma mark - Detail Requests

- (void) detailforPlace: (NSString *) placeName performSelector: (NSString *) selector;{
    NSString *url = [NSString stringWithFormat:@"%@restaurants/%@", wenoAPI, placeName];
    
    [WOFRequestHelper performRequestForUrl:url withSuccess:^(NSDictionary *response) {
        [delegate requestDidFinishRequestingData:response performSelector:selector];
    } withFailure:^(NSError *error) {
        [delegate requestErrorInConnection];
    }];
}

- (void) detailforRestaurantWithID: (NSInteger) restID performSelector: (NSString *) selector{
    NSString *url = [NSString stringWithFormat:@"%@restaurants/%ld", wenoAPI, (long) restID];
    
    [WOFRequestHelper performRequestForUrl:url withSuccess:^(NSDictionary *response) {
        [delegate requestDidFinishRequestingData:response performSelector:selector];
    } withFailure:^(NSError *error) {
        [delegate requestErrorInConnection];
    }];
}

- (void) detailforFood: (NSString *) foodName performSelector: (NSString *) selector;{
    NSString *url = [NSString stringWithFormat:@"%@foods/%@", wenoAPI, foodName];
    
    [WOFRequestHelper performRequestForUrl:url withSuccess:^(NSDictionary *response) {
        [delegate requestDidFinishRequestingData:response performSelector:selector];
    } withFailure:^(NSError *error) {
        [delegate requestErrorInConnection];
    }];
}

#pragma mark - Master request
+ (void) performRequestForUrl: (NSString *) url withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *strUrl=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    manager = [AFHTTPRequestOperationManager manager];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    [manager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];
}

#pragma mark Block methods

+ (void)getMenuForRestaurant: (NSInteger) restId withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure{
    NSString *url = [NSString stringWithFormat:@"%@foodcategory/%ld", wenoAPI, (long)restId];
    [WOFRequestHelper performRequestForUrl:url withSuccess:^(NSDictionary *response) {
        success(response);
    } withFailure:^(NSError *error) {
        failure(error);
    }];
}

@end
