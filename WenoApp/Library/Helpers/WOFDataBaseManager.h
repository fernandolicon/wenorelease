//
//  WOFObjectCoreDataHelper.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 10/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <Foundation/Foundation.h>
@class WOFRestaurantCoreData;
@class WOFFoodCoreData;
@class WOFRestaurant;
@class WOFFood;
@class RLMResults;

@interface WOFDataBaseManager : NSObject

#pragma mark - Restaurant methods
+ (void) createFavoriteWithRestaurant: (WOFRestaurant *) favoriteRestaurant withSuccess:(void (^)())success;
+ (void) removeFavoriteRestaurant: (WOFRestaurant *) removeRestaurant;
+ (RLMResults *) findAllRestaurants;
+ (BOOL) isRestaurantwithIDFavorite: (NSInteger) restID;

#pragma mark - Food methods
+ (void) createFavoriteWithFood: (WOFFood *) favoriteFood withSuccess:(void (^)())success;
+ (void) removeFavoriteFood: (WOFFood *) removeFood;
+ (RLMResults *) findAllFoods;
+ (BOOL) isFoodwithIDFavorite: (NSInteger) foodID;
+ (NSData *) foodPictureWithID: (NSInteger) foodID;

#pragma mark - Database methods
+ (void) eraseAllInfoinDataBaseWithSuccess:(void (^)())success;

@end