//
//  WOFViewHelper.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 10/10/15.
//  Copyright © 2015 Weno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface WOFViewHelper : NSObject

+ (void) presentDefaultAlertViewWithTitle: (NSString *) title withMessage: (NSString *) message inViewController: (UIViewController *) viewController;

+ (NSString *) nameForType: (NSString *) type;

@end
