//
//  WOFObjectCoreDataHelper.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 10/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFDataBaseManager.h"

#import "WOFRestaurant.h"
#import "WOFFood.h"
#import <Realm/Realm.h>

@implementation WOFDataBaseManager

#pragma  mark - Restaurant methods
+ (void) createFavoriteWithRestaurant: (WOFRestaurant *) favoriteRestaurant withSuccess:(void (^)())success {
    RLMRealm *defaultRealm = [RLMRealm defaultRealm];
    favoriteRestaurant.primaryPhotoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:favoriteRestaurant.primaryPhoto]];
    favoriteRestaurant.bannerPhotoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:favoriteRestaurant.bannerPhoto]];
    [defaultRealm transactionWithBlock:^{
        [defaultRealm addObject:favoriteRestaurant];
        success();
    }];
}

+ (void) removeFavoriteRestaurant: (WOFRestaurant *) removeRestaurant{
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    NSString *predicate = [NSString stringWithFormat:@"objectID = %ld", removeRestaurant.objectID];
    RLMResults *restaurant = [WOFRestaurant objectsWhere:predicate];
    
    [realm transactionWithBlock:^{
        [realm deleteObject:restaurant[0]];
    }];
}

+ (RLMResults *) findAllRestaurants{
    return [WOFRestaurant allObjects];
}

+ (BOOL) isRestaurantwithIDFavorite: (NSInteger) restID{
    NSString *predicate = [NSString stringWithFormat:@"objectID = %ld", restID];
    RLMResults *restaurant = [WOFRestaurant objectsWhere:predicate];
    return (restaurant.count != 0) ? YES : NO;
}

#pragma mark - Food methods
+ (void) createFavoriteWithFood: (WOFFood *) favoriteFood withSuccess:(void (^)())success{
    RLMRealm *defaultRealm = [RLMRealm defaultRealm];
    favoriteFood.primaryPhotoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:favoriteFood.primaryPhoto]];
    [defaultRealm transactionWithBlock:^{
        [defaultRealm addObject:favoriteFood];
        success();
    }];
}

+ (void) removeFavoriteFood: (WOFFood *) removeFood {
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    NSString *predicate = [NSString stringWithFormat:@"objectID = %ld", removeFood.objectID];
    RLMResults *food = [WOFFood objectsWhere:predicate];
    
    [realm transactionWithBlock:^{
        [realm deleteObject:food[0]];
    }];
}

+ (RLMResults *) findAllFoods{
    return [WOFFood allObjects];
}

+ (BOOL) isFoodwithIDFavorite: (NSInteger) foodID{
    NSString *predicate = [NSString stringWithFormat:@"objectID = %ld", foodID];
    RLMResults *restaurant = [WOFFood objectsWhere:predicate];
    return (restaurant.count != 0) ? YES : NO;
}

+ (NSData *) foodPictureWithID: (NSInteger) foodID{
    NSString *predicate = [NSString stringWithFormat:@"objectID = %ld", foodID];
    RLMResults *foods = [WOFFood objectsWhere:predicate];
    WOFFood *food = foods[0];
    
    return food.primaryPhotoData;
}

#pragma mark - Database methods
+ (void) eraseAllInfoinDataBaseWithSuccess:(void (^)())success {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteAllObjects];
    [realm commitWriteTransaction];
    success();
}

@end
