//
//  WOFRestaurantTableViewCell.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 24/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WOFRestaurantTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *informationTitle;
@property (weak, nonatomic) IBOutlet UILabel *informationText;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIView *separatorLine;

@end