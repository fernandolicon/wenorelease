//
//  WOFTypeListTableViewCell.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 28/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WOFTypeListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *typeListText;

@end
