//
//  WOFTypeListTableViewCell.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 28/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFTypeListTableViewCell.h"

@implementation WOFTypeListTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
