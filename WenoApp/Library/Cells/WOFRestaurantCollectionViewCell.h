//
//  WOFRestaurantCollectionViewCell.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 29/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WOFObject;

@interface WOFRestaurantCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bannerImage;
@property (weak, nonatomic) IBOutlet UILabel *objectName;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *descriptionOne;
@property (weak, nonatomic) IBOutlet UILabel *descriptionTwo;
@property CGFloat cellHeight;

- (void) createCellWithObject: (WOFObject *) cellObject;

@end
