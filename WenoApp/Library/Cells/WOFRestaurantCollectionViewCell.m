//
//  WOFRestaurantCollectionViewCell.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 29/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFRestaurantCollectionViewCell.h"
#import "WOFObject.h"
#import "WOFRestaurant.h"
#import "WOFFood.h"
#import <Haneke/Haneke.h>

@implementation WOFRestaurantCollectionViewCell

- (void) createCellWithObject: (WOFObject *) cellObject{
    self.objectName.text = cellObject.name;
    UIImage *whiteImage = [UIImage imageNamed:@"whiteImage"];
    
    if (cellObject.isRestaurant) {
        [self.bannerImage hnk_setImageFromURL:[NSURL URLWithString:cellObject.bannerPhoto] placeholder:whiteImage];
        [self.logoImage setHidden:NO];
        [self.logoImage hnk_setImageFromURL: [NSURL URLWithString:cellObject.primaryPhoto] placeholder:whiteImage];
        WOFRestaurant *provisionalRest = (WOFRestaurant *) cellObject;
        self.descriptionOne.text = provisionalRest.typeMenu;
        self.descriptionTwo.text = provisionalRest.zone;
    }else{
        [self.bannerImage hnk_setImageFromURL: [NSURL URLWithString: cellObject.primaryPhoto] placeholder:whiteImage];
        [self.logoImage setHidden:YES];
        WOFFood *provisionalFood = (WOFFood *) cellObject;
        self.descriptionOne.text = [NSString stringWithFormat:@"$%.2f", provisionalFood.price];
        self.descriptionTwo.text = provisionalFood.restaurantName;
    }
    
    [self.contentView.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.contentView.layer setBorderWidth:0.5f];
}

- (void)prepareForReuse{
    UIImage *whiteImage = [UIImage imageNamed:@"whiteImage"];
    [self.bannerImage hnk_cancelSetImage];
    [self.logoImage hnk_cancelSetImage];
    self.bannerImage.image = whiteImage;
    self.logoImage.image = whiteImage;
    self.descriptionOne.text = @"";
    self.descriptionTwo.text = @"";
}

@end
