//
//  WOFCameraView.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 13/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFCameraView.h"
#import <AVFoundation/AVFoundation.h>

@implementation WOFCameraView

+ (Class)layerClass
{
    return [AVCaptureVideoPreviewLayer class];
}

- (AVCaptureSession *)session
{
    return [(AVCaptureVideoPreviewLayer *)[self layer] session];
}

- (void)setSession:(AVCaptureSession *)session
{
    [(AVCaptureVideoPreviewLayer *)[self layer] setSession:session];
}

@end
