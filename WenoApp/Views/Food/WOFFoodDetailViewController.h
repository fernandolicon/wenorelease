//
//  WOFFoodDetailViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 24/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFBaseViewController.h"
@class WOFFood;

@protocol WOFFoodDetailDelegate <NSObject>
- (void) favoriteChanged;
@end

@interface WOFFoodDetailViewController : WOFBaseViewController

@property (nonatomic, weak) id <WOFFoodDetailDelegate> delegate;

@property (strong, nonatomic) WOFFood *detailFood;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *bannerImage;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurView;
@property (weak, nonatomic) IBOutlet UITableView *infoTableView;
@property BOOL isFavorite;

- (IBAction)dismissView:(id)sender;
- (IBAction)markAsFavorite:(id)sender;

@end
