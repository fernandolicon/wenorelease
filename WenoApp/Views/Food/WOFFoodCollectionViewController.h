//
//  FoodCollectionViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 1/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WOFFoodCollectionViewController : UICollectionViewController

@property NSInteger restaurantID;
@property (strong, nonatomic) NSString *restaurantName;

- (IBAction)tryAgain:(id)sender;

@end
