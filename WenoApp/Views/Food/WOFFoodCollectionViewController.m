//
//  FoodCollectionViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 1/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFFoodCollectionViewController.h"
#import "WOFFoodDetailViewController.h"
#import "WOFRequestHelper.h"
#import "Haneke.h"
#import "WOFRestaurantCollectionViewCell.h"
#import "WOFFood.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface WOFFoodCollectionViewController () <WOFRequestHelperDelegate>{
    NSMutableArray *restaurantMenu;
    WOFRequestHelper *requestHelper;
    WOFFood *selectedFood;
    CGFloat cellSide;
    MBProgressHUD *progressHUD;
    NSMutableDictionary *imagesDictionary;
    IBOutlet UIView *notFoundView;
    __weak IBOutlet UILabel *notFoundText;
    __weak IBOutlet UIButton *notFoundButton;
}

@end

@implementation WOFFoodCollectionViewController

static NSString * const reuseIdentifier = @"foodCollectionView";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //To get the correct height we need to calculate it using the 7:3 aspect ratio we're using
    CGFloat collectionWidth = self.collectionView.frame.size.width;
    cellSide = (collectionWidth / 2.0f) - 10;
    
    //Set Not Found view size
    CGRect newFrame = notFoundView.frame;
    newFrame.size.width = self.view.frame.size.width;
    newFrame.size.height = newFrame.size.height;
    [notFoundView setFrame:newFrame];
    
    imagesDictionary = [[NSMutableDictionary alloc] init];
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    progressHUD.labelText = @"Cargando";
    
    requestHelper = [[WOFRequestHelper alloc] init];
    requestHelper.delegate = self;
    
    [self requestData];
    
    restaurantMenu = [[NSMutableArray alloc] init];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    self.navigationItem.title = [NSString stringWithFormat:@"Menú de %@", self.restaurantName];
}

#pragma mark - Populate data

- (void) populateFoodListforResponse: (NSDictionary *) responseObject{
    NSLog(@"%@", responseObject);
    NSDictionary *foodDictionary = [responseObject objectForKey:@"foods"];
    for (NSDictionary *food in foodDictionary) {
        [restaurantMenu addObject:[[WOFFood alloc] initForMenuWithDictionary:food forRestaurantName:self.restaurantName]];
    }
    
    [restaurantMenu sortUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [[(WOFObject*)a name] lowercaseString];
        NSString *second = [[(WOFObject*)b name] lowercaseString];
        return [first compare:second];
    }];
    
    if (restaurantMenu.count == 0) {
        notFoundText.text = @"No se encontró menú para este restaurante.";
        [notFoundButton setHidden:YES];
        [self.view addSubview:notFoundView];
    }
    
    [progressHUD hide:YES];
    [self.collectionView reloadData];
}

#pragma mark CollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return restaurantMenu.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WOFRestaurantCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    WOFObject *provisionalObject = [restaurantMenu objectAtIndex:indexPath.row];
    [cell createCellWithObject:provisionalObject];
    
    return cell;
}

#pragma mark CollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    selectedFood = [restaurantMenu objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"showFood" sender:self];
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeihgt = cellSide + (cellSide * 0.25f);
    CGSize cell = CGSizeMake(cellSide, cellHeihgt);
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark collection view cell paddings
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 2.5, 5, 2.5); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showFood"]) {
        WOFFoodDetailViewController *nextVC = [segue destinationViewController];
        nextVC.detailFood = selectedFood;
    }
}

#pragma mark - Request Delegate

- (void) requestData{
    [progressHUD show:YES];
    [requestHelper menuforRestaurantwithId:self.restaurantID performSelector:@"populateFoodListforResponse:"];
}


- (void) requestDidFinishRequestingData:(NSDictionary *)responseDictionary performSelector:(NSString *)selector{
    [self performSelector:NSSelectorFromString(selector) withObject:responseDictionary afterDelay:0.0];
}

- (void) requestErrorInConnection{
    [self showConnectionPerformedSuccessful:NO];
}

- (void) showConnectionPerformedSuccessful: (BOOL) wasSuccessful{
    [progressHUD hide:YES];
    
    if (wasSuccessful) {
        [self.collectionView reloadData];
    }else{
        [self.view addSubview:notFoundView];
    }
}

- (IBAction)tryAgain:(id)sender {
    [notFoundView removeFromSuperview];
    [self requestData];
}
@end
