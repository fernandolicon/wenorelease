//
//  WOFFoodDetailViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 24/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFFoodDetailViewController.h"
#import "WOFDataBaseManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <Haneke/Haneke.h>
#import "WOFRestaurantTableViewCell.h"
#import "WOFrestaurant.h"
#import "WOFRestaurantDetailViewController.h"
#import "WOFViewHelper.h"
#import "WOFRequestHelper.h"
#import "WOFFood.h"

@interface WOFFoodDetailViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, WOFRequestHelperDelegate>{
    WOFRestaurant *foodRestaurant;
    CGFloat lastOffset;
    NSArray *titles;
    WOFRequestHelper *requestHelper;
    MBProgressHUD *progressHUD;
    NSArray *descriptions;
    NSString *moreInfo;
    BOOL isRegisteredUser;
    BOOL connectionWasSuccessful;
}

@end

@implementation WOFFoodDetailViewController

@synthesize detailFood;
@synthesize isFavorite;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self visualSetup];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    lastOffset = self.infoTableView.contentOffset.y;
    
    requestHelper = [[WOFRequestHelper alloc] init];
    requestHelper.delegate = self;
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    
    //No registered users can't save favorites
    NSString *logInMethod = [[NSUserDefaults standardUserDefaults] objectForKey:@"logInMethod"];
    isRegisteredUser = [logInMethod isEqualToString:@"NoMethod"] ? NO : YES;
    
    isFavorite = [WOFDataBaseManager isFoodwithIDFavorite:detailFood.objectID];
    
    titles = [[NSArray alloc] init];
    descriptions = [[NSArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewWillAppear:(BOOL)animated{
    [self createTextsforTable];
    
    self.navigationItem.title = detailFood.name;
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - Set data

- (void) createTextsforTable{
    moreInfo = [NSString stringWithFormat:@"$%.2f\nTiempo: %@\n", detailFood.price, detailFood.timeCook];
    
    if (!isFavorite) {
        [self.bannerImage hnk_setImageFromURL: [NSURL URLWithString:self.detailFood.primaryPhoto]];
    }else{
        NSData *foodImage = [WOFDataBaseManager foodPictureWithID:detailFood.objectID];
        [self.bannerImage setImage:[UIImage imageWithData:foodImage]];
    }
}

#pragma mark - Visual setup
-(void)visualSetup{
    self.infoTableView.contentInset = UIEdgeInsetsMake(self.bannerImage.bounds.size.height, 0, 0, 0);
    self.infoTableView.tableFooterView = [[UIView alloc] init];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.infoTableView.contentInset = UIEdgeInsetsMake(self.bannerImage.bounds.size.height, 0, 100, 0);
}

#pragma mark - Scroll view delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y > lastOffset) {
        self.blurView.alpha += .015;
    }else{
        self.blurView.alpha -= .015;
    }
    
    if (scrollView.contentOffset.y < -200) {
        self.blurView.alpha = 0.05;
    }
    
    if (scrollView.contentOffset.y < 10) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        self.backButton.hidden = NO;
        
    }else if (scrollView.contentOffset.y > 20){
        self.backButton.hidden = YES;
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    
    
    lastOffset = scrollView.contentOffset.y;
}

#pragma mark - Table view datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WOFRestaurantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"foodDetail"];
    switch (indexPath.row) {
        case 0:
            cell.informationTitle.text = detailFood.restaurantName;
            [cell.informationText setHidden:YES];
            [cell.separatorLine setHidden:NO];
            [cell.favoriteButton setImage:[UIImage imageNamed:@"return_icon"] forState:UIControlStateNormal];
            [cell.favoriteButton setUserInteractionEnabled:NO];
            break;
        case 1:
            cell.informationTitle.text = detailFood.name;
            cell.informationText.text = detailFood.objectDescription;
            isFavorite ? [cell.favoriteButton setImage:[UIImage imageNamed:@"favorite_fill"] forState:UIControlStateNormal] : nil;
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            break;
        case 2:
            cell.informationTitle.text = @"Precio";
            cell.informationText.text = moreInfo;
            [cell.favoriteButton setHidden:YES];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        default:
            break;
    }
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        [self requestRestaurantData];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 44;
    }
    NSString *cellText = [self stringforRow:indexPath.row];
    CGFloat height = [cellText boundingRectWithSize:CGSizeMake(tableView.frame.size.width - 25, 150) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:nil context:nil].size.height;
    return (height < 50) ? 150 : height + 150;
}

- (NSString *) stringforRow: (NSInteger) row{
    switch (row) {
        case 1:
            return detailFood.objectDescription;
            break;
        case 2:
            return moreInfo;
            break;
        default:
            return nil;
            break;
    }
}

- (IBAction)dismissView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)markAsFavorite:(id)sender {
    UIButton *favoriteButton = (UIButton *) sender;
    
    if (isRegisteredUser) {
        if (!isFavorite) {
            [progressHUD showAnimated:YES whileExecutingBlock:^{
                [WOFDataBaseManager createFavoriteWithFood:detailFood withSuccess:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [progressHUD hide:YES];
                        [favoriteButton setImage:[UIImage imageNamed:@"favorite_fill"] forState:UIControlStateNormal];
                        [self.delegate favoriteChanged];
                        [WOFViewHelper presentDefaultAlertViewWithTitle:@"Listo" withMessage:@"Se ha guardado tu favorito." inViewController:self];
                        isFavorite = YES;
                    });
                }];
            }];
        }else{
            [WOFDataBaseManager removeFavoriteFood:detailFood];
            [self.delegate favoriteChanged];
            [favoriteButton setImage:[UIImage imageNamed:@"favorite_empty"] forState:UIControlStateNormal];
            isFavorite = NO;
        }
    }else{
        [WOFViewHelper presentDefaultAlertViewWithTitle:@"No estás registrado" withMessage:@"Sólo los usuarios registrados pueden guardar favoritos." inViewController:self];
    }
    
}

#pragma mark - Requests

- (void) requestRestaurantData{
    progressHUD.labelText = @"Cargando";
    [progressHUD show:YES];
    [requestHelper detailforRestaurantWithID:detailFood.restaurantID performSelector:@"createRestaurantWithDictionary:"];
}

- (void) createRestaurantWithDictionary: (NSDictionary *) responseDictionary{
    NSDictionary *restaurantDictionary = [responseDictionary objectForKey:@"restaurant"];
    foodRestaurant = [[WOFRestaurant alloc] initWithAlternativeDictionary:restaurantDictionary];
    [self showConnectionPerformedSuccessful:YES];
}

- (void)requestDidFinishRequestingData:(NSDictionary *)responseDictionary performSelector:(NSString *)selector{
    [self performSelector:NSSelectorFromString(selector) withObject:responseDictionary afterDelay:0.0];
}

- (void)requestErrorInConnection{
    [self showConnectionPerformedSuccessful:NO];
}

- (void) showConnectionPerformedSuccessful: (BOOL) wasSuccessful{
    [progressHUD hide:YES];
    
    if (wasSuccessful) {
        [self performSegueWithIdentifier:@"showRestaurantServingFood" sender:nil];
    }else{
        [self showErrorAlert];
    }
}

- (void) showErrorAlert{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Hubo un error al contactar al servidor, intenta más tarde." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showRestaurantServingFood"]) {
        WOFRestaurantDetailViewController *nextVC = (WOFRestaurantDetailViewController *) [segue destinationViewController];
        nextVC.detailRestaurant = foodRestaurant;
    }
}

@end
