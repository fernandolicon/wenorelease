//
//  WOFProfileTableViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 2/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WOFProfileTableViewController : UITableViewController

@end
