//
//  WOFProfileTableViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 2/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFProfileTableViewController.h"
#import "WOFProfileTableViewCell.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "WOFUser.h"
#import "WOFDataBaseManager.h"

@interface WOFProfileTableViewController () {
    WOFUser *activeUser;
    MBProgressHUD *progressHUD;
}

@end

@implementation WOFProfileTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"user_info.txt"];
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    progressHUD.labelText = @"Cerrando sesión";
    
    activeUser = [NSKeyedUnarchiver unarchiveObjectWithFile:appFile];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        WOFProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"profileCell" forIndexPath:indexPath];
        if (activeUser.userImage) {
            cell.userProfilePicture.image = [UIImage imageWithData:activeUser.userImage];
        }else{
            NSData *userImage = [NSData dataWithContentsOfURL:activeUser.userImageURL];
            cell.userProfilePicture.image = [UIImage imageWithData:userImage];
        }
        cell.userName.text = activeUser.name;
        [cell setUserInteractionEnabled:NO];
        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"otherCell" forIndexPath:indexPath];
    
    cell.textLabel.font = [UIFont fontWithName:@"Muli" size:17.0f];
    cell.textLabel.text = @"Cerrar sesión";
    cell.textLabel.textColor = [UIColor redColor];
    
    return cell;
}

#pragma mark - Table view delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 100.0f;
    }

    return 44.0f;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30.0f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return (section == 0) ? @"Mi perfil" : @"";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1) {
        [self logOutUser];
    }
}

#pragma mark - Actions

- (void) logOutUser{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Cerrar sesión" message:@"¿Estás seguro de querer cerrar sesión? Esto borrará toda tu información y tu lista de favoritos." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Cerrar sesión" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [progressHUD show:YES];
        [WOFDataBaseManager eraseAllInfoinDataBaseWithSuccess:^{
            NSString *logInMethod = [[NSUserDefaults standardUserDefaults] objectForKey:@"logInMethod"];
            [self signOutUserforSocialNetwork:logInMethod];
        }];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
    
    [alert addAction:defaultAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - LogOut Actions

- (void) signOutUserforSocialNetwork: (NSString *) logInMethod{
    [progressHUD hide:YES];
    if ([logInMethod isEqualToString:@"Google"]) {
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        [[GIDSignIn sharedInstance] signOut];
    }else if ([logInMethod isEqualToString:@"Facebook"]){
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"user_info.txt"];
    [[NSFileManager defaultManager] removeItemAtPath:appFile error:nil];
    
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"logInMethod"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self performSegueWithIdentifier:@"userLogout" sender:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
