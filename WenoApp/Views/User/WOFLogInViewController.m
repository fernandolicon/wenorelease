//
//  FirstViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 11/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFLogInViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "WOFUser.h"
#import "WOFViewHelper.h"
#import "Settings.h"
#import "Haneke.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface WOFLogInViewController () <GIDSignInDelegate, GIDSignInUIDelegate>{
    MBProgressHUD *progressHUD;
    __weak IBOutlet UIImageView *logoImage;
}


@end

@implementation WOFLogInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"LogIn";
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    progressHUD.labelText = @"Registrando";
    
    //Google LogIn
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.clientID = kClientId;
    signIn.scopes = @[ @"profile", @"email" ];
    signIn.delegate = self;
    signIn.uiDelegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - LogIn Method Delegates

- (void) setUserDefaultsforLogInMethod: (NSString *) logInMethod{
    [[NSUserDefaults standardUserDefaults] setObject:logInMethod forKey:@"logInMethod"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [progressHUD hide:YES];
}

- (void) errorWithAuth{
    [WOFViewHelper presentDefaultAlertViewWithTitle:@"Error" withMessage:@"Hubo algún error con la autenticación, intenta nuevamente." inViewController:self];
}

- (IBAction)logInFacebook:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"email"]  fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [self errorWithAuth];
        } else if (result.isCancelled) {
            [self errorWithAuth];
        } else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                [self getFacebookData];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (1 * (NSEC_PER_SEC))), dispatch_get_main_queue(), ^{
                    [self performSegueWithIdentifier:@"userLoggedIn" sender:nil];
                });
            }
        }
    }];
}

- (IBAction)logInGoogle:(id)sender {
    [[GIDSignIn sharedInstance] signIn];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    if (!error) {
        [self getGoogleDataForUser:user];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (1 * (NSEC_PER_SEC))), dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"userLoggedIn" sender:nil];
        });
    }else{
        [self errorWithAuth];
    }
}

- (IBAction)continueWithoutRegister:(id)sender {
    [self setUserDefaultsforLogInMethod:@"NoMethod"];
    [self performSegueWithIdentifier:@"userLoggedIn" sender:nil];
}

#pragma mark - User Data

- (void) getFacebookData{
    [progressHUD show:YES];
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, NSDictionary *result, NSError *error) {
             if (!error) {
                 WOFUser *facebookUser = [[WOFUser alloc] initWithFacebookResponse:result];
                 [self registerUserInfo:facebookUser];
                 [self setUserDefaultsforLogInMethod:@"Facebook"];
             }else{
                 [self errorWithAuth];
             }
         }];
        }
}

- (void) getGoogleDataForUser: (GIDGoogleUser *) user {
    [progressHUD show:YES];
    WOFUser *googleUser = [[WOFUser alloc] initWithGoogleProfile:user];
    [self registerUserInfo:googleUser];
    [self setUserDefaultsforLogInMethod:@"Google"];}

- (void) registerUserInfo: (WOFUser *) loggedUser{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"user_info.txt"];
    [NSKeyedArchiver archiveRootObject:loggedUser toFile:appFile];
}

@end
