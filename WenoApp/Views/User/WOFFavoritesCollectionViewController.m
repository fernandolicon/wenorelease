//
//  WOFFavoritesCollectionViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 10/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFFavoritesCollectionViewController.h"
#import "WOFDataBaseManager.h"
#import "WOFRestaurantDetailViewController.h"
#import "WOFFoodDetailViewController.h"
#import "WOFRestaurantCollectionViewCell.h"
#import "WOFObject.h"
#import "WOFFood.h"
#import "WOFRestaurant.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Haneke.h"
#import "WOFFoodDetailViewController.h"
#import "WOFRestaurantDetailViewController.h"

@interface WOFFavoritesCollectionViewController ()<WOFFoodDetailDelegate, WOFRestaurantDetailDelegate>{
    RLMResults *favorites;
    CGFloat cellSide;
    MBProgressHUD *progressHUD;
    WOFRestaurant *selectedRestaurant;
    WOFFood *selectedFood;
    NSMutableDictionary *imagesDictionary;
    IBOutlet UIView *notFoundView;
    __weak IBOutlet UILabel *notFoundText;
    NSInteger selectionIndex;
}

@end

@implementation WOFFavoritesCollectionViewController

@synthesize selectedRestaurants;

static NSString * const reuseIdentifier = @"favoriteCell";

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    CGFloat collectionWidth = self.collectionView.frame.size.width;
    cellSide = (collectionWidth / 2.0f) - 10;
    
    //Set Not Found view size
    CGRect newFrame = notFoundView.frame;
    newFrame.size.width = self.view.frame.size.width;
    newFrame.size.height = newFrame.size.height;
    [notFoundView setFrame:newFrame];
    
    imagesDictionary = [[NSMutableDictionary alloc] init];
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    
    [progressHUD show:YES];
    [self populateFavoriteList];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    NSString *titleSelection = selectedRestaurants ? @"Lugares favoritos" : @"Comidas favoritas";
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@", titleSelection];
}

#pragma mark - Populate data

- (void) populateFavoriteList{
    [progressHUD hide:YES];
    favorites = selectedRestaurants ? [WOFDataBaseManager findAllRestaurants] : [WOFDataBaseManager findAllFoods];
    
    if (favorites.count == 0) {
        [self.view addSubview:notFoundView];
    }
}

#pragma mark CollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return favorites.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WOFRestaurantCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    WOFObject *provisionalObject = [favorites objectAtIndex:indexPath.row];
    [cell createCellWithObject:provisionalObject];
    
    return cell;

}

//favoriteFood

#pragma mark CollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    WOFObject *selectedObject = [favorites objectAtIndex:indexPath.row];
    selectionIndex = indexPath.row;
    
    if (selectedObject.isRestaurant) {
        selectedRestaurant = (WOFRestaurant *) selectedObject;
        [self performSegueWithIdentifier:@"favoriteRestaurant" sender:self];
    }else{
        selectedFood = (WOFFood *) selectedObject;
        [self performSegueWithIdentifier:@"favoriteFood" sender:self];
    }
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeihgt = cellSide + (cellSide * 0.25f);
    CGSize cell = CGSizeMake(cellSide, cellHeihgt);
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark collection view cell paddings
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 2.5, 5, 2.5); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

#pragma mark - Delete favorite delegates

- (void)favoriteChanged{
    [self populateFavoriteList];
    [self.collectionView reloadData];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"favoriteRestaurant"]) {
        WOFRestaurantDetailViewController *nextVC = [segue destinationViewController];
        nextVC.detailRestaurant = selectedRestaurant;
        nextVC.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"favoriteFood"]) {
        WOFFoodDetailViewController *nextVC = [segue destinationViewController];
        nextVC.detailFood = selectedFood;
        nextVC.delegate = self;
    }
}

@end
