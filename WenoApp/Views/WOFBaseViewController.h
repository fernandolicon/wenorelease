//
//  BaseViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 18/12/15.
//  Copyright © 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/Analytics.h>
#import "WOFViewHelper.h"

@interface WOFBaseViewController : UIViewController

@end
