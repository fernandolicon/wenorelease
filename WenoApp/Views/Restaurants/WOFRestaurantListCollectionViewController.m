//
//  WOFRestaurantListCollectionViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 29/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFRestaurantListCollectionViewController.h"
#import "WOFRestaurantDetailViewController.h"
#import "Haneke.h"
#import "WOFRequestHelper.h"
#import "WOFRestaurant.h"
#import "WOFRestaurantCollectionViewCell.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface WOFRestaurantListCollectionViewController () <WOFRequestHelperDelegate>{
    WOFRequestHelper *requestHelper;
    NSMutableArray *restaurants;
    CGFloat cellSide;
    MBProgressHUD *progressHUD;
    WOFRestaurant *selectedRestaurant;
    IBOutlet UIView *notFoundView;
    __weak IBOutlet UILabel *notFoundText;
    __weak IBOutlet UIButton *notFoundButton;
}

@end

@implementation WOFRestaurantListCollectionViewController

static NSString * const reuseIdentifier = @"restaurantCollectionCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //To get the correct height we need to calculate it using the 7:3 aspect ratio we're using
    CGFloat collectionWidth = self.collectionView.frame.size.width;
    cellSide = (collectionWidth / 2.0f) - 10;
    
    //Set Not Found view size
    CGRect newFrame = notFoundView.frame;
    newFrame.size.width = self.view.frame.size.width;
    newFrame.size.height = newFrame.size.height;
    [notFoundView setFrame:newFrame];
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    progressHUD.labelText = @"Cargando";
    
    requestHelper = [[WOFRequestHelper alloc] init];
    requestHelper.delegate = self;
    
    [self requestData];
    
    restaurants = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    self.navigationItem.title = [NSString stringWithFormat:@"Listado de %@", self.typeSelection];
}

#pragma mark - Populate data

- (void) populateRestaurantListforResponse: (NSDictionary *) responseObject{
    for (NSDictionary *restaurant in responseObject) {
        [restaurants addObject:[[WOFRestaurant alloc] initWithDictionary:restaurant]];
    }
    
    [restaurants sortUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [[(WOFObject*)a name] lowercaseString];
        NSString *second = [[(WOFObject*)b name] lowercaseString];
        return [first compare:second];
    }];
    
    if (restaurants.count == 0) {
        notFoundText.text = @"No se encontraron restaurantes.";
        [notFoundButton setHidden:YES];
        [self.view addSubview:notFoundView];
    }
    
    [progressHUD hide:YES];
    [self.collectionView reloadData];
}

#pragma mark CollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return restaurants.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WOFRestaurantCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    WOFRestaurant *provisionalRest = [restaurants objectAtIndex:indexPath.row];
    cell.objectName.text = provisionalRest.name;
    cell.descriptionOne.text = provisionalRest.typeMenu;
    cell.descriptionTwo.text = provisionalRest.zone;
    [cell.bannerImage hnk_setImageFromURL: [NSURL URLWithString:provisionalRest.bannerPhoto]];
    [cell.logoImage hnk_setImageFromURL: [NSURL URLWithString:provisionalRest.primaryPhoto]];
    [cell.contentView.layer setBorderColor:[UIColor blackColor].CGColor];
    [cell.contentView.layer setBorderWidth:0.5f];
    
    return cell;
}

#pragma mark CollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    selectedRestaurant = [restaurants objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"restaurantDetail" sender:self];
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeihgt = cellSide + (cellSide * 0.25f);
    CGSize cell = CGSizeMake(cellSide, cellHeihgt);
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark collection view cell paddings
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 2.5, 5, 2.5); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0;
}

#pragma mark - Request Delegate

- (void) requestData{
    [progressHUD show:YES];
    [requestHelper listofPlacesBy:self.typeSelection forType:self.selectionRoute performSelector:@"populateRestaurantListforResponse:"];
}


- (void) requestDidFinishRequestingData:(NSDictionary *)responseDictionary performSelector:(NSString *)selector{
    [self performSelector:NSSelectorFromString(selector) withObject:responseDictionary afterDelay:0.0];
}

- (void) requestErrorInConnection{
    [self showConnectionPerformedSuccessful:NO];
}

- (void) showConnectionPerformedSuccessful: (BOOL) wasSuccessful{
    [progressHUD hide:YES];
    
    if (wasSuccessful) {
        [self.collectionView reloadData];
    }else{
        [self.view addSubview:notFoundView];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"restaurantDetail"]) {
        WOFRestaurantDetailViewController *nextVC = [segue destinationViewController];
        nextVC.detailRestaurant = selectedRestaurant;
    }
}

- (IBAction)tryAgain:(id)sender {
    [notFoundView removeFromSuperview];
    [self requestData];
}
@end
