//
//  WOFRestaurantMenuViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 31/10/15.
//  Copyright © 2015 Weno. All rights reserved.
//

#import "WOFRestaurantMenuViewController.h"
#import "WOFRequestHelper.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "WOFRestaurant.h"
#import "WOFViewHelper.h"

@interface WOFRestaurantMenuViewController () <UITableViewDataSource, UITableViewDelegate>{
    __weak IBOutlet UITableView *menuTableView;
    NSMutableArray *restaurantMenu;
    IBOutlet UIView *notFoundView;
    MBProgressHUD *progressHUD;
}

@end

@implementation WOFRestaurantMenuViewController

@synthesize restaurant;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Set Not Found view size
    CGRect newFrame = notFoundView.frame;
    newFrame.size.width = self.view.frame.size.width;
    newFrame.size.height = newFrame.size.height;
    [notFoundView setFrame:newFrame];
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    progressHUD.labelText = @"Cargando";
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Data methods

- (void) populateArrayOfDataWithDictionary: (NSDictionary *) response{
    /*for (NSArray *typeFood in response) {
    }*/
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return restaurantMenu.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"typeListCell" forIndexPath:indexPath];
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Request

- (void) requestData{
    [progressHUD show:YES];
    [WOFRequestHelper getMenuForRestaurant:restaurant.objectID withSuccess:^(NSDictionary *response) {
        [self populateArrayOfDataWithDictionary:response];
    } withFailure:^(NSError *error) {
        [self showConnectionPerformedSuccessful:NO];
    }];
}

- (void) showConnectionPerformedSuccessful: (BOOL) wasSuccessful{
    [progressHUD hide:YES];
    
    if (wasSuccessful) {
        [notFoundView removeFromSuperview];
        [menuTableView reloadData];
    }else{
        [self.view addSubview:notFoundView];
    }
}

- (IBAction)reloadData:(id)sender {
    
}

@end
