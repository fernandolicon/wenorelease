//
//  WOFRestaurantDetailViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 23/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "WOFRestaurantDetailViewController.h"
//#import "WOFRestaurantMenuViewController.h"
#import "WOFRestaurantListCollectionViewController.h"
#import "WOFRestaurantTableViewCell.h"
#import "WOFDataBaseManager.h"
#import "WOFFoodCollectionViewController.h"
#import "WOFViewHelper.h"
#import "WOFRestaurant.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Haneke.h"

@interface WOFRestaurantDetailViewController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>{
    CGFloat lastOffset;
    MBProgressHUD *progressHUD;
    NSString *contactInfo;
    NSString *phoneNumber;
    NSString *moreInfo;
    NSString *wiFiPassword;
    BOOL isRegisteredUser;
}

@end

@implementation WOFRestaurantDetailViewController

@synthesize detailRestaurant;
@synthesize isFavorite;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self visualSetup];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //No registered users can't save favorites
    NSString *logInMethod = [[NSUserDefaults standardUserDefaults] objectForKey:@"logInMethod"];
    isRegisteredUser = [logInMethod isEqualToString:@"NoMethod"] ? NO : YES;
    
    //Google analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:[GAIFields customDimensionForIndex:1]
           value:detailRestaurant.name];
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    progressHUD.labelText = @"Guardando";
    
    isFavorite = [WOFDataBaseManager isRestaurantwithIDFavorite:detailRestaurant.objectID];
    
    lastOffset = self.infoTableView.contentOffset.y;
    
    [self setRestaurantDetail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    self.navigationItem.title = detailRestaurant.name;
    self.restaurantText.text = detailRestaurant.name;
}

#pragma mark - Set data

- (void) setRestaurantDetail{
    contactInfo = [NSString stringWithFormat:@"%@\n%@", detailRestaurant.address, detailRestaurant.typeMenu];
    phoneNumber = detailRestaurant.phone;
    NSString *deliveryString = detailRestaurant.hasDelivery ? @"Servicio a domicilio: Sí" : @"Servicio a domicilio: No";
    NSString *reservationString = detailRestaurant.hasReservations ? @"Permite reservaciones: Sí" : @"Permite reservaciones: No";
    NSString *eventInString = detailRestaurant.eventInPlace ? @"Eventos privados: Sí" : @"Eventos privados: No";
    NSString *eventOutString = detailRestaurant.eventInYourPlace ? @"Servicio a eventos: Sí" : @"Servicio a eventos: No";
    moreInfo = [NSString stringWithFormat:@"Vestimenta: %@\nZona: %@\n%@\n%@\n%@\n%@\nTipo de pago: %@", detailRestaurant.codeDress, detailRestaurant.zone, deliveryString, reservationString, eventInString, eventOutString, detailRestaurant.payType];
    
    wiFiPassword = detailRestaurant.wifiPassword;
    
    if (!isFavorite) {
        [self.bannerImage hnk_setImageFromURL: [NSURL URLWithString:detailRestaurant.bannerPhoto]];
        [self.logoImage hnk_setImageFromURL: [NSURL URLWithString:detailRestaurant.primaryPhoto]];
    }else{
        if (detailRestaurant.bannerPhotoData) {
            self.bannerImage.image = [UIImage imageWithData:detailRestaurant.bannerPhotoData];
        } else {
            [self.bannerImage hnk_setImageFromURL: [NSURL URLWithString:detailRestaurant.bannerPhoto]];
        }
        
        if (detailRestaurant.primaryPhotoData) {
            self.logoImage.image = [UIImage imageWithData:detailRestaurant.primaryPhotoData];
        } else {
            [self.logoImage hnk_setImageFromURL: [NSURL URLWithString:detailRestaurant.primaryPhoto]];
        }
    }
}

#pragma mark Visual setup
-(void)visualSetup{
    self.infoTableView.contentInset = UIEdgeInsetsMake(self.bannerImage.bounds.size.height, 0, 0, 0);
    self.infoTableView.tableFooterView = [[UIView alloc] init];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.infoTableView.contentInset = UIEdgeInsetsMake(self.bannerImage.bounds.size.height, 0, 100, 0);
}

#pragma mark - Scroll view delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (scrollView.contentOffset.y > lastOffset) {
        self.blurView.alpha += .015;
    }else{
        self.blurView.alpha -= .015;
    }
    
    if (scrollView.contentOffset.y < -200) {
        self.blurView.alpha = 0.05;
    }
    
    if (scrollView.contentOffset.y < 10) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        self.backButton.hidden = NO;
        
    }else if (scrollView.contentOffset.y > 20){
        self.backButton.hidden = YES;
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    
    
    lastOffset = scrollView.contentOffset.y;
}

#pragma mark - Table view datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WOFRestaurantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"restaurantDetail"];
    switch (indexPath.row) {
        case 0:
            cell.informationTitle.text = @"Ver menú";
            cell.informationText.text = @"Descubre el menú de este local";
            [cell.favoriteButton setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
            [cell.favoriteButton setUserInteractionEnabled:NO];
            [cell.separatorLine setHidden:NO];
            break;
        case 1:
            cell.informationTitle.text = @"Agregar a mis Wenos";
            cell.informationText.text = @"Guarda tus favoritos.";
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            isFavorite ? [cell.favoriteButton setImage:[UIImage imageNamed:@"favorite_fill"] forState:UIControlStateNormal] : [cell.favoriteButton setImage:[UIImage imageNamed:@"favorite_empty"] forState:UIControlStateNormal];
            break;
        case 2:
            cell.informationTitle.text = @"Dirección (Tap para ir al mapa)";
            cell.informationText.text = [NSString stringWithFormat:@"%@",contactInfo];
            [cell.favoriteButton setImage:[UIImage imageNamed:@"contact_icon"] forState:UIControlStateNormal];
            [cell.favoriteButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
            break;
        case 3:
            cell.informationTitle.text = @"Teléfono (Tap para llamar)";
            [cell.favoriteButton setImage:[UIImage imageNamed:@"phone_icon"] forState:UIControlStateNormal];
            cell.informationText.text = [NSString stringWithFormat:@"%@", phoneNumber];
            break;
        case 4:
            cell.informationTitle.text = @"Nuestra historia";
            [cell.favoriteButton setImage:[UIImage imageNamed:@"history_icon"] forState:UIControlStateNormal];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.informationText.text = detailRestaurant.objectDescription;
            break;
        case 5:
            cell.informationTitle.text = @"Más información";
            [cell.favoriteButton setImage:[UIImage imageNamed:@"more_info_icon"] forState:UIControlStateNormal];
            cell.informationText.text = moreInfo;
            break;
        case 6:
            cell.informationTitle.text = @"WiFi (Tap para copiar clave)";
            [cell.favoriteButton setImage:[UIImage imageNamed:@"wifi_icon"] forState:UIControlStateNormal];
            [cell.favoriteButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
            cell.informationText.text = [NSString stringWithFormat:@"Nombre de la red: %@\nClave WiFi: %@", detailRestaurant.wifiName,wiFiPassword];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            break;
        default:
            break;
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"showMenu" sender:nil];
            break;
        case 2:{
            CLLocationCoordinate2D restaurantCoordinate = CLLocationCoordinate2DMake(detailRestaurant.latitude, detailRestaurant.longitude);
            MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:restaurantCoordinate addressDictionary:nil];
            MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
            item.name = detailRestaurant.name;
            [item openInMapsWithLaunchOptions:nil];
            
        }
        case 3:{
            NSString *restaurantPhone = [@"tel://" stringByAppendingString:phoneNumber];
            restaurantPhone = [restaurantPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:restaurantPhone]];
            break;
        }
        case 6:
            [UIPasteboard generalPasteboard].string = wiFiPassword;
            [WOFViewHelper presentDefaultAlertViewWithTitle:@"Se copió la contraseña" withMessage:@"Se copió la contraseña del WiFi en tu portapapeles." inViewController:self];
            break;
        default:
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (NSString *) stringforRow: (NSInteger) row{
    switch (row) {
        case 1:
            return contactInfo;
            break;
        case 2:
            return detailRestaurant.objectDescription;
            break;
        case 3:
            return moreInfo;
        default:
            return nil;
            break;
    }
}

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    
    /// the height of the table itself.
    float height = self.infoTableView.frame.size.height + self.infoTableView.contentOffset.y;
    /// the bounds of the table.
    /// it's strange that the origin of the table view is actually the top-left of the table.
    CGRect tableBounds = CGRectMake(0, 0, self.infoTableView.bounds.size.width, height);
    return CGRectContainsPoint(tableBounds, point);
    
}

#pragma mark - Actions

- (IBAction)dismissView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)markFavorite:(id)sender {
    UIButton *favoriteButton = (UIButton *) sender;
    if (isRegisteredUser) {
        if (!isFavorite) {
            [progressHUD showAnimated:YES whileExecutingBlock:^{
                [WOFDataBaseManager createFavoriteWithRestaurant:detailRestaurant withSuccess:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [progressHUD hide:YES];
                        [favoriteButton setImage:[UIImage imageNamed:@"favorite_fill"] forState:UIControlStateNormal];
                        [self.delegate favoriteChanged];
                        [WOFViewHelper presentDefaultAlertViewWithTitle:@"Listo" withMessage:@"Se ha guardado tu favorito." inViewController:self];
                        isFavorite = YES;
                    });
                }];
            }];
        }else{
            [WOFDataBaseManager removeFavoriteRestaurant:detailRestaurant];
            [self.delegate favoriteChanged];
            [favoriteButton setImage:[UIImage imageNamed:@"favorite_empty"] forState:UIControlStateNormal];
            isFavorite = NO;
        }
    }else{
        [WOFViewHelper presentDefaultAlertViewWithTitle:@"No estás registrado" withMessage:@"Sólo los usuarios registrados pueden guardar favoritos." inViewController:self];
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showMenu"]) {
        /*WOFRestaurantMenuViewController *nextVC = [segue destinationViewController];
         nextVC.restaurant = detailRestaurant;*/
        WOFFoodCollectionViewController *nextVC = [segue destinationViewController];
        nextVC.restaurantID = detailRestaurant.objectID;
        nextVC.restaurantName = detailRestaurant.name;
    }
}

@end
