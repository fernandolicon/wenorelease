//
//  WOFRestaurantDetailViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 23/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFBaseViewController.h"
@class WOFRestaurant;

@protocol WOFRestaurantDetailDelegate <NSObject>
- (void) favoriteChanged;
@end

@interface WOFRestaurantDetailViewController : WOFBaseViewController;

@property (nonatomic, weak) id <WOFRestaurantDetailDelegate> delegate;

@property (strong, nonatomic) WOFRestaurant *detailRestaurant;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *bannerImage;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *restaurantText;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurView;
@property (weak, nonatomic) IBOutlet UITableView *infoTableView;
@property BOOL isFavorite;

- (IBAction)dismissView:(id)sender;
- (IBAction)markFavorite:(id)sender;

@end
