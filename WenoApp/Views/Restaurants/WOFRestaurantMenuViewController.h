//
//  WOFRestaurantMenuViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 31/10/15.
//  Copyright © 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFBaseViewController.h"
@class WOFRestaurant;

@interface WOFRestaurantMenuViewController : WOFBaseViewController

@property (strong, nonatomic) WOFRestaurant *restaurant;

@end
