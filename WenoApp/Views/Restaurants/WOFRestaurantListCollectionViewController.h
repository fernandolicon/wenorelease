//
//  WOFRestaurantListCollectionViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 29/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFBaseViewController.h"

@interface WOFRestaurantListCollectionViewController : UICollectionViewController

@property (strong, nonatomic) NSString *typeSelection;
@property (strong, nonatomic) NSString *selectionRoute;

- (IBAction)tryAgain:(id)sender;

@end
