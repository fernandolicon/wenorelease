//
//  BaseViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 18/12/15.
//  Copyright © 2015 Weno. All rights reserved.
//

#import "WOFBaseViewController.h"

@interface WOFBaseViewController ()

@end

@implementation WOFBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *name = [NSString stringWithFormat:@"Pattern~%@", self.title];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    NSLog(@"Name: %@", self.title);
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
