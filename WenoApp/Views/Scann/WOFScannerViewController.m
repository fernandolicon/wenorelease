//
//  WOFScannerViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 13/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFScannerViewController.h"
#import "WOFViewHelper.h"
#import <MBProgressHUD/MBProgressHUD.h>

static void * CapturingStillImageContext = &CapturingStillImageContext;
static void * RecordingContext = &RecordingContext;
static void * SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;

@interface WOFScannerViewController ()<G8TesseractDelegate, WOFRequestHelperDelegate>{
    G8Tesseract *tesseract;
    WOFRequestHelper *requestHelper;
    MBProgressHUD *progressHUD;
    WOFRestaurant *restaurantFound;
    BOOL searchInProgress;
    __weak IBOutlet WOFCameraView *cameraPreview;
    __weak IBOutlet UIButton *stillButton;
    __weak IBOutlet UIImageView *scannArea;
}


@end

@implementation WOFScannerViewController


// Utilities.
@synthesize deviceAuthorized;
@synthesize sessionRunningAndDeviceAuthorized;
@synthesize lockInterfaceRotation;
@synthesize runtimeErrorHandlingObserver;

- (BOOL)isSessionRunningAndDeviceAuthorized
{
    return [[self session] isRunning] && [self isDeviceAuthorized];
}

+ (NSSet *)keyPathsForValuesAffectingSessionRunningAndDeviceAuthorized
{
    return [NSSet setWithObjects:@"session.running", @"deviceAuthorized", nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    tesseract = [[G8Tesseract alloc] initWithLanguage:@"eng+spa"];
    tesseract.delegate = self;
    tesseract.charWhitelist = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    requestHelper = [[WOFRequestHelper alloc] init];
    requestHelper.delegate = self;
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    progressHUD.labelText = @"Cargando";

    
    // Create the AVCaptureSession
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    [self setSession:session];
    
    // Setup the preview view
    [cameraPreview setSession:session];
    
    // Check for device authorization
    [self checkDeviceAuthorizationStatus];
    
    // In general it is not safe to mutate an AVCaptureSession or any of its inputs, outputs, or connections from multiple threads at the same time.
    // Why not do all of this on the main queue?
    // -[AVCaptureSession startRunning] is a blocking call which can take a long time. We dispatch session setup to the sessionQueue so that the main queue isn't blocked (which keeps the UI responsive).
    
    dispatch_queue_t sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
    [self setSessionQueue:sessionQueue];
    
    dispatch_async(sessionQueue, ^{
        [self setBackgroundRecordingID:UIBackgroundTaskInvalid];
        
        NSError *error = nil;
        
        AVCaptureDevice *videoDevice = [WOFScannerViewController deviceWithMediaType:AVMediaTypeVideo preferringPosition:AVCaptureDevicePositionBack];
        AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
        
        if (error)
        {
            NSLog(@"%@", error);
        }
        
        if ([session canAddInput:videoDeviceInput])
        {
            [session addInput:videoDeviceInput];
            [self setVideoDeviceInput:videoDeviceInput];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Why are we dispatching this to the main queue?
                // Because AVCaptureVideoPreviewLayer is the backing layer for AVCamPreviewView and UIView can only be manipulated on main thread.
                // Note: As an exception to the above rule, it is not necessary to serialize video orientation changes on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
 //               [[(AVCaptureVideoPreviewLayer *) [cameraPreview layer] connection] setVi]
                [[(AVCaptureVideoPreviewLayer *)[cameraPreview layer] connection] setVideoOrientation:AVCaptureVideoOrientationPortrait];
            });
        }
        
        AVCaptureDevice *audioDevice = [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] firstObject];
        AVCaptureDeviceInput *audioDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:&error];
        
        if (error)
        {
            NSLog(@"%@", error);
        }
        
        if ([session canAddInput:audioDeviceInput])
        {
            [session addInput:audioDeviceInput];
        }
        
        AVCaptureStillImageOutput *stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        if ([session canAddOutput:stillImageOutput])
        {
            [stillImageOutput setOutputSettings:@{AVVideoCodecKey : AVVideoCodecJPEG}];
            [session addOutput:stillImageOutput];
            [self setStillImageOutput:stillImageOutput];
        }
    });
}

- (void)viewWillAppear:(BOOL)animated
{
    dispatch_async([self sessionQueue], ^{
        [self addObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:SessionRunningAndDeviceAuthorizedContext];
        [self addObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:CapturingStillImageContext];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
        
        __weak WOFScannerViewController *weakSelf = self;
        [self setRuntimeErrorHandlingObserver:[[NSNotificationCenter defaultCenter] addObserverForName:AVCaptureSessionRuntimeErrorNotification object:[self session] queue:nil usingBlock:^(NSNotification *note) {
            WOFScannerViewController *strongSelf = weakSelf;
            dispatch_async([strongSelf sessionQueue], ^{
                // Manually restarting the session since it must have been stopped due to an error.
                [[strongSelf session] startRunning];
                //[[strongSelf recordButton] setTitle:NSLocalizedString(@"Record", @"Recording button record title") forState:UIControlStateNormal];
            });
        }]];
        [[self session] startRunning];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    dispatch_async([self sessionQueue], ^{
        [[self session] stopRunning];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
        [[NSNotificationCenter defaultCenter] removeObserver:[self runtimeErrorHandlingObserver]];
        
        [self removeObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" context:SessionRunningAndDeviceAuthorizedContext];
        [self removeObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" context:CapturingStillImageContext];
    });
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    // Disable autorotation of the interface when recording is in progress.
    return ![self lockInterfaceRotation];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [[(AVCaptureVideoPreviewLayer *)[cameraPreview layer] connection] setVideoOrientation:(AVCaptureVideoOrientation)toInterfaceOrientation];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == CapturingStillImageContext)
    {
        BOOL isCapturingStillImage = [change[NSKeyValueChangeNewKey] boolValue];
        
        if (isCapturingStillImage)
        {
            [self runStillImageCaptureAnimation];
        }
    }
    else if (context == SessionRunningAndDeviceAuthorizedContext)
    {
        BOOL isRunning = [change[NSKeyValueChangeNewKey] boolValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isRunning)
            {
                [stillButton setEnabled:YES];
            }
            else
            {
                [stillButton setEnabled:NO];
            }
        });
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark Actions

- (IBAction)takeStillPicture:(id)sender
{
    [progressHUD show:YES];
    dispatch_async([self sessionQueue], ^{
        
        // Update the orientation on the still image output video connection before capturing.
        [[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:[[(AVCaptureVideoPreviewLayer *)[cameraPreview layer] connection] videoOrientation]];
        
        // Flash set to Auto for Still Capture
        [WOFScannerViewController setFlashMode:AVCaptureFlashModeAuto forDevice:[[self videoDeviceInput] device]];
        
        // Capture a still image.
        [[self stillImageOutput] captureStillImageAsynchronouslyFromConnection:[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
            
            if (imageDataSampleBuffer)
            {
                
                NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                UIImage *image = [[UIImage alloc] initWithData:imageData];
                UIImage *croppedImage = [self cropImage:image cropRect:scannArea.frame.size];
                [self recognizeLettersfromImage:croppedImage];
            }
        }];
    });
}

- (IBAction)cancelScann:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)insertManually:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ingresa el codigo" message:@"Son las tres letras dentro del recuadro." preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Código";
    }];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Buscar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        UITextField *codeField = [alert.textFields objectAtIndex:0];
        NSString *insertedCode = [codeField.text uppercaseString];
        insertedCode = [insertedCode stringByReplacingOccurrencesOfString:@" " withString:@""];
        [self performRequestforCode:insertedCode];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){}];
    
    [alert addAction:defaultAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)focusAndExposeTap:(UIGestureRecognizer *)gestureRecognizer
{
    if (searchInProgress) {
        [progressHUD hide:YES];
        searchInProgress = NO;
    }else{
        CGPoint devicePoint = [(AVCaptureVideoPreviewLayer *)[cameraPreview layer] captureDevicePointOfInterestForPoint:[gestureRecognizer locationInView:[gestureRecognizer view]]];
        [self focusWithMode:AVCaptureFocusModeAutoFocus exposeWithMode:AVCaptureExposureModeAutoExpose atDevicePoint:devicePoint monitorSubjectAreaChange:YES];
    }
}



- (void)subjectAreaDidChange:(NSNotification *)notification
{
    CGPoint devicePoint = CGPointMake(.5, .5);
    [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:devicePoint monitorSubjectAreaChange:NO];
}

#pragma mark Device Configuration

- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    dispatch_async([self sessionQueue], ^{
        AVCaptureDevice *device = [[self videoDeviceInput] device];
        NSError *error = nil;
        if ([device lockForConfiguration:&error])
        {
            if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode])
            {
                [device setFocusMode:focusMode];
                [device setFocusPointOfInterest:point];
            }
            if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:exposureMode])
            {
                [device setExposureMode:exposureMode];
                [device setExposurePointOfInterest:point];
            }
            [device setSubjectAreaChangeMonitoringEnabled:monitorSubjectAreaChange];
            [device unlockForConfiguration];
        }
        else
        {
            NSLog(@"%@", error);
        }
    });
}

+ (void)setFlashMode:(AVCaptureFlashMode)flashMode forDevice:(AVCaptureDevice *)device
{
    if ([device hasFlash] && [device isFlashModeSupported:flashMode])
    {
        NSError *error = nil;
        if ([device lockForConfiguration:&error])
        {
            [device setFlashMode:flashMode];
            [device unlockForConfiguration];
        }
        else
        {
            NSLog(@"%@", error);
        }
    }
}

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
    AVCaptureDevice *captureDevice = [devices firstObject];
    
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
        {
            captureDevice = device;
            break;
        }
    }
    
    return captureDevice;
}

#pragma mark OCR

- (UIImage *)cropImage: (UIImage *) originalImage cropRect:(CGSize)size {
    double refWidth = CGImageGetWidth(originalImage.CGImage);
    double refHeight = CGImageGetHeight(originalImage.CGImage);
    
    //Multiply by 3 because it works. Really, I guess it has something to do with the image resolution.
    double actualWidth = size.width * 3.0;
    double actualHeight = size.height * 3.0;
    
    double x = (refWidth - actualWidth) / 2.0;
    double y = (refHeight - actualHeight) / 2.0;
    
    CGRect cropRect = CGRectMake(x, y, actualHeight, actualWidth);
    CGImageRef imageRef = CGImageCreateWithImageInRect([originalImage CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:0.0 orientation:UIImageOrientationUp];
    
    return cropped;
}

- (void)recognizeLettersfromImage:(UIImage *)image {
    UIImage *rotatedImage = [image rotateInDegrees:-90];
    //tesseract.image = [rotatedImage g8_grayScale];
    tesseract.image = rotatedImage;
    [tesseract recognize];
    NSString *recognizedText = [[tesseract recognizedText] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    recognizedText = [recognizedText stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (recognizedText.length <= 4) {
        [self performRequestforCode:recognizedText];
    }else{
        [self pictureDidNotCaptureRight];
    }
}

#pragma mark - Request

- (void) performRequestforCode: (NSString *) codeText{
    searchInProgress = YES;
    [requestHelper searchRestaurantforCode:codeText performSelector:@"assignRestaurantforResponse:"];
}

- (void) requestDidFinishRequestingData:(NSDictionary *)responseDictionary performSelector:(NSString *)selector{
    [self performSelector:NSSelectorFromString(selector) withObject:responseDictionary afterDelay:0.0];
}

- (void) assignRestaurantforResponse: (NSDictionary *) responseObject{
    NSDictionary *restaurantDictionary = [responseObject objectForKey:@"restaurant"];
    if ([restaurantDictionary isEqual:[NSNull null]]) {
        [self pictureDidNotFetchRestaurant];
    }else{
        restaurantFound = [[WOFRestaurant alloc] initWithAlternativeDictionary:restaurantDictionary];
        [self showConnectionPerformedSuccessful:YES];
    }
}

- (void) requestErrorInConnection{
    [WOFViewHelper presentDefaultAlertViewWithTitle:@"Hubo un error" withMessage:@"Hubo un error al tratar de contactar el servidor. Vuelve a intentarlo más tarde." inViewController:self];
    [self showConnectionPerformedSuccessful:NO];
}

- (void) pictureDidNotCaptureRight{
    [WOFViewHelper presentDefaultAlertViewWithTitle:@"No se capturó correctamente" withMessage:@"Hubo un error al capturar el código, intentalo nuevamente." inViewController:self];
    [self showConnectionPerformedSuccessful:NO];
}

- (void) pictureDidNotFetchRestaurant{
    [WOFViewHelper presentDefaultAlertViewWithTitle:@"No existe el restaurante" withMessage:@"No se encontró ningún restaurante que coincidiera con el código, intentalo nuevamente." inViewController:self];
    [self showConnectionPerformedSuccessful:NO];
}

- (void) showConnectionPerformedSuccessful: (BOOL) wasSuccessful{
    [progressHUD hide:YES];
    
    if (wasSuccessful && searchInProgress) {
        [self.delegate restaurantFound:restaurantFound];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    searchInProgress = NO;
}

#pragma mark UI

- (void)runStillImageCaptureAnimation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[cameraPreview layer] setOpacity:0.0];
        [UIView animateWithDuration:.25 animations:^{
            [[cameraPreview layer] setOpacity:1.0];
        }];
    });
}

- (void)checkDeviceAuthorizationStatus
{
    NSString *mediaType = AVMediaTypeVideo;
    
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
        if (granted)
        {
            //Granted access to mediaType
            [self setDeviceAuthorized:YES];
        }
        else
        {
            //Not granted access to mediaType
            dispatch_async(dispatch_get_main_queue(), ^{
                [WOFViewHelper presentDefaultAlertViewWithTitle:@"No se puede acceder a la cámara."  withMessage:@"Hubo un error al tratar de acceder a la cámara. Revisa los permisos de la aplicación, por favor." inViewController:self];
                [self setDeviceAuthorized:NO];
            });
        }
    }];
}

@end
