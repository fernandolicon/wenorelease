//
//  WOFScannerViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 13/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFBaseViewController.h"
#import <TesseractOCR/TesseractOCR.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "WOFCameraView.h"
#import "WOFRestaurant.h"
#import "WOFRequestHelper.h"
#import "NYXImagesKit.h"

@protocol WOFScannerDelegate <NSObject>
- (void) restaurantFound: (WOFRestaurant *) foundRestaurant;
@end

@interface WOFScannerViewController : WOFBaseViewController
//Delegate
@property (nonatomic, weak) id <WOFScannerDelegate> delegate;

// Session management.
@property (nonatomic) dispatch_queue_t sessionQueue; // Communicate with the session and other session objects on this queue.
@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (nonatomic) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;

// Utilities.
@property (nonatomic) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, getter = isDeviceAuthorized) BOOL deviceAuthorized;
@property (nonatomic, readonly, getter = isSessionRunningAndDeviceAuthorized) BOOL sessionRunningAndDeviceAuthorized;
@property (nonatomic) BOOL lockInterfaceRotation;
@property (nonatomic) id runtimeErrorHandlingObserver;

- (IBAction)takeStillPicture:(id)sender;
- (IBAction)focusAndExposeTap:(UIGestureRecognizer *)gestureRecognizer;
- (IBAction)cancelScann:(id)sender;
- (IBAction)insertManually:(id)sender;

@end
