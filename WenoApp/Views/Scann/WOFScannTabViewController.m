//
//  WOFScannTabViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 24/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFScannTabViewController.h"
#import "WOFRestaurant.h"
#import "WOFRestaurantDetailViewController.h"
#import "WOFScannerViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface WOFScannTabViewController () <WOFScannerDelegate>{
    WOFRestaurant *selectedRestaurant;
    MBProgressHUD *progressHUD;
}

@end

@implementation WOFScannTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    
    selectedRestaurant = [[WOFRestaurant alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}


#pragma mark - Scanner delegate

- (void)restaurantFound:(WOFRestaurant *)foundRestaurant{
    selectedRestaurant = foundRestaurant;
    [self performSegueWithIdentifier:@"scannedRestaurantDetail" sender:nil];
}

#pragma mark - Actions

- (IBAction)scannMenu:(id)sender {
    [self performSegueWithIdentifier:@"scannRestaurantMenu" sender:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"scannedRestaurantDetail"]) {
        WOFRestaurantDetailViewController *nextVC = [segue destinationViewController];
        nextVC.detailRestaurant = selectedRestaurant;
    }
    
    if ([segue.identifier isEqualToString:@"scannRestaurantMenu"]) {
        WOFScannerViewController *nextVC = [segue destinationViewController];
        nextVC.delegate = self;
    }
}

@end
