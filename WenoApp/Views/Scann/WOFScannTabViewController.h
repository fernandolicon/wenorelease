//
//  WOFScannTabViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 24/7/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFBaseViewController.h"

@interface WOFScannTabViewController : WOFBaseViewController

- (IBAction)scannMenu:(id)sender;

@end
