//
//  WOFDashboardViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 7/21/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFBaseViewController.h"

@interface WOFDashboardViewController : WOFBaseViewController

- (IBAction)shoeSearchView:(id)sender;

@end
