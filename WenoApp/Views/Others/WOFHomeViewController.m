//
//  SecondViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 11/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFHomeViewController.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <GooglePlus/GooglePlus.h>
#import "Settings.h"

@interface WOFHomeViewController () <GPPSignInDelegate>{
    __weak IBOutlet UILabel *nameLbl;
}

@end

@implementation WOFHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    bool googleToken = [[GPPSignIn sharedInstance] hasAuthInKeychain];
    if (!googleToken) {
        GPPSignIn *signIn = [GPPSignIn sharedInstance];
        signIn.shouldFetchGooglePlusUser = YES;
        signIn.shouldFetchGoogleUserEmail = YES;
        signIn.clientID = kClientId;
        signIn.scopes = @[ kGTLAuthScopePlusLogin ];
        signIn.delegate = self;
        [signIn trySilentAuthentication];
    }else {
        [self getFacebookData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - User Data

- (void) getFacebookData{
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, NSDictionary *result, NSError *error) {
             if (!error) {
                 //NSLog(@"fetched user:%@", [result objectForKey:@"id"]);
                 //Getting Facebook profile picture: "http://graph.facebook.com/\(userID)/picture?type=large"
             }
         }];
    }
}

- (void) getGoogleData{
    GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
    plusService.retryEnabled = YES;
    
    [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
    
    GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
    
    [plusService executeQuery:query
            completionHandler:^(GTLServiceTicket *ticket,
                                GTLPlusPerson *person,
                                NSError *error) {
                if (error) {
                    GTMLoggerError(@"Error: %@", error);
                } else {
                    // Retrieve the display name and "about me" text
                    NSString *description = [NSString stringWithFormat:
                                             @"%@", person.displayName];
                    nameLbl.text = description;
                }
            }];
}

#pragma mark - Google Delegate

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    if (!error) {
        //GPPSignIn *signIn = [GPPSignIn sharedInstance];
        //NSLog(@"%@", signIn.authentication.userEmail);
        [self getGoogleData];
    }else{
        NSLog(@"Received error %@ and auth object %@",error, auth);
    }
}

- (IBAction)showScanner:(id)sender {
    [self performSegueWithIdentifier:@"showScanner" sender:nil];
}
@end
