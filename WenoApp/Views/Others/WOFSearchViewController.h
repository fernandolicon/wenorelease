//
//  WOFSearchViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 26/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFBaseViewController.h"

typedef enum userFilters
{
    ALL,
    REST,
    FOOD
} UserFilters;

@interface WOFSearchViewController : WOFBaseViewController

@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *swipe;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *searchFilter;
@property (weak, nonatomic) IBOutlet UIView *notFoundView;
@property (weak, nonatomic) IBOutlet UICollectionView *searchCollectionView;

@end
