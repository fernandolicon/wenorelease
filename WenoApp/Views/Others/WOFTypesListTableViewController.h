//
//  WOFTypesListTableViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 29/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WOFTypesListTableViewController : UITableViewController 

@property (nonatomic, strong) NSDictionary *selection;

- (IBAction)tryAgain:(id)sender;

@end
