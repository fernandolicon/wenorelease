//
//  WOFDashboardViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 7/21/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFDashboardViewController.h"
#import "WOFTypeListTableViewCell.h"
#import "WOFTypesListTableViewController.h"
#import <KIImagePager/KIImagePager.h>
#import "WOFRestaurant.h"

@interface WOFDashboardViewController ()<UITableViewDataSource, UITableViewDelegate, KIImagePagerDelegate, KIImagePagerDataSource>{
    NSArray *typesofList;
    NSDictionary *selection;
    WOFRestaurant *detailRestaurant;
    NSString *heightString;
    __weak IBOutlet UITableView *typesTable;
    __weak IBOutlet KIImagePager *bannerPager;
    __weak IBOutlet UISearchBar *foodSearchBar;
}

@end

@implementation WOFDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    // Do any additional setup after loading the view.
    
    bannerPager.pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    bannerPager.slideshowTimeInterval = 5.0f;
    [bannerPager setImageCounterDisabled:YES];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wenotitle"]];
    
    [typesTable setScrollEnabled:NO];

    [self populateData];
}

- (void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void) bannerString{
    CGFloat deviceHeight = self.view.frame.size.height;
    heightString = [[NSString alloc] init];
    
    if (deviceHeight > 700) {
        heightString = @"6p";
    }else if (deviceHeight > 600){
        heightString = @"6";
    }else if (deviceHeight > 500){
        heightString = @"5";
    }else{
        heightString = @"4";
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) populateData{
    //NSDictionary *foodByType = @{@"name" : @"Productos por tipo de comida", @"image" : @"scan_banner", @"shortName" : @"Tipo de Comida"};
    NSDictionary *zones = @{@"name" : @"Negocios por zonas", @"image" : @"zones_banner", @"route" : @"zones", @"shortName" : @"Zonas", @"typeRoute" : @"zone"};
    NSDictionary *typeFood = @{@"name" : @"Negocios por giro", @"image" : @"typefood_banner", @"route" : @"types", @"shortName" : @"Giro", @"typeRoute" : @"type"};
    //NSDictionary *foodParks = @{@"name" : @"Colectivos", @"image" : @"scan_banner", @"route" : @"foodpark", @"shortName" : @"Colectivos"};
    
    //typesofList = [[NSArray alloc] initWithObjects:foodByType, zones, typeFood, foodParks, nil];
    typesofList = [[NSArray alloc] initWithObjects:zones, typeFood, nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return typesofList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat tableHeight = typesTable.frame.size.height;
    CGFloat height = tableHeight / 2.0f;
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WOFTypeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"discoverCell" forIndexPath:indexPath];
    
    NSDictionary *provisionalDic = [typesofList objectAtIndex:indexPath.row];
    cell.typeListText.text = [provisionalDic objectForKey:@"name"];
    cell.backgroundImage.image = [UIImage imageNamed:[provisionalDic objectForKey:@"image"]];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    selection = [typesofList objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"showType" sender:nil];
}

#pragma mark - Image Pager Methods

- (NSArray *) arrayWithImages:(KIImagePager *)pager{
    [self bannerString];
    
    NSString *banner1 = [NSString stringWithFormat:@"banner1%@", heightString];
    NSString *banner2 = [NSString stringWithFormat:@"banner2%@", heightString];
    NSString *banner3 = [NSString stringWithFormat:@"banner3%@", heightString];
    NSString *banner4 = [NSString stringWithFormat:@"banner4%@", heightString];
    
    NSArray *imageArrays = @[[UIImage imageNamed:banner1], [UIImage imageNamed:banner2], [UIImage imageNamed:banner3], [UIImage imageNamed:banner4]];
    
    return imageArrays;
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager*)pager
{
    //return UIViewContentModeScaleAspectFit;
    return UIViewContentModeScaleToFill;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showType"]) {
        WOFTypesListTableViewController *nextVC = [segue destinationViewController];
        nextVC.selection = selection;
    }
}

- (IBAction)shoeSearchView:(id)sender {
    [self performSegueWithIdentifier:@"searchBarSelected" sender:nil];
}
@end
