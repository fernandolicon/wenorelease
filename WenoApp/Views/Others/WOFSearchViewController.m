//
//  WOFSearchViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 26/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFSearchViewController.h"
#import "WOFRestaurantDetailViewController.h"
#import "WOFFoodDetailViewController.h"
#import "WOFViewHelper.h"
#import "WOFRequestHelper.h"
#import "Haneke.h"
#import "WOFObject.h"
#import "WOFRestaurant.h"
#import "WOFFood.h"
#import "WOFRestaurantCollectionViewCell.h"
#import "WOFScannerViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface WOFSearchViewController ()<UISearchBarDelegate, WOFRequestHelperDelegate, UICollectionViewDataSource, UICollectionViewDelegate, WOFScannerDelegate>{
    UserFilters selectedFilter;
    NSMutableArray *tableDataArray;
    NSMutableArray *restaurantArray;
    NSMutableArray *foodArray;
    WOFRequestHelper *requestHelper;
    NSMutableDictionary *imagesDictionary;
    WOFRestaurant *selectedRestaurant;
    CGFloat cellSide;
    WOFFood *selectedFood;
    UIApplication *networkIdentifier;
    NSInteger foodValue;
    NSInteger restaurantValue;
    MBProgressHUD *progressHUD;
    UIBarButtonItem *scanButton;
    BOOL wasScanned;
    __weak IBOutlet UILabel *notFoundViewText;
}

@end

@implementation WOFSearchViewController

static NSString * const reuseIdentifier = @"searchCollectionCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.searchCollectionView setBackgroundColor:[UIColor whiteColor]];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    scanButton = self.navigationItem.rightBarButtonItem;
    
    //To get the correct height we need to calculate it using the 7:3 aspect ratio we're using
    CGFloat collectionWidth = self.view.frame.size.width;
    cellSide = (collectionWidth / 2.0f) - 10;
    
    [self.notFoundView addGestureRecognizer:self.swipe];
    [self.notFoundView addGestureRecognizer:self.tapGesture];
    [self.swipe addTarget:self action:@selector(dismissKeyboard)];
    [self.tapGesture addTarget:self action:@selector(dismissKeyboard)];
    
    networkIdentifier = [UIApplication sharedApplication];
    requestHelper = [[WOFRequestHelper alloc] init];
    requestHelper.delegate = self;
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    progressHUD.labelText = @"Cargando";
    
    imagesDictionary = [[NSMutableDictionary alloc] init];
    tableDataArray = [[NSMutableArray alloc] init];
    foodArray = [[NSMutableArray alloc] init];
    restaurantArray = [[NSMutableArray alloc] init];
    
    [self.notFoundView setHidden:YES];
    
    selectedFilter = ALL;
    wasScanned = NO;
}

- (void)viewDidAppear:(BOOL)animated{
    [self.searchBar becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - Populate data

- (void) populateInfoWithResponse: (NSDictionary *) responseObject{
    
    [tableDataArray removeAllObjects];
    [restaurantArray removeAllObjects];
    [foodArray removeAllObjects];
    
    NSArray *restaurantsJSONs = [responseObject objectForKey:@"restaurant"];
    NSArray *foodJSONs = [responseObject objectForKey:@"food"];
    
    if (restaurantsJSONs.count != 0) {
        restaurantArray = [self createRestaurantsArrayfromArray:restaurantsJSONs];
    }
    
    if (foodJSONs.count != 0) {
        foodArray = [self createFoodsArrayfromArray:foodJSONs];
    }
    
    tableDataArray = [[NSMutableArray alloc] initWithArray:foodArray];
    [tableDataArray addObjectsFromArray:restaurantArray];
    
    [tableDataArray sortUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [[(WOFObject*)a name] lowercaseString];
        NSString *second = [[(WOFObject*)b name] lowercaseString];
        return [first compare:second];
    }];
    
    long totalCount = (long)foodArray.count + restaurantArray.count;
    NSString *totalObjects = [NSString stringWithFormat:@"Todo (%ld)", totalCount];
    NSString *totalPlaces = [NSString stringWithFormat:@"Lugares (%ld)", (long) restaurantArray.count];
    NSString *totalFood = [NSString stringWithFormat:@"Productos (%ld)", (long) foodArray.count];
    
    [self.searchFilter setTitle:totalObjects forSegmentAtIndex:0];
    [self.searchFilter setTitle:totalPlaces forSegmentAtIndex:1];
    [self.searchFilter setTitle:totalFood forSegmentAtIndex:2];
    
    if (tableDataArray.count == 0) {
        [self showConnectionPerformedSuccessful:NO];
    }else{
        [self showConnectionPerformedSuccessful:YES];
    }
}

- (NSMutableArray *) createRestaurantsArrayfromArray: (NSArray *) restaurantsJSONs{
    NSMutableArray *restsArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *restaurantDic in restaurantsJSONs) {
        WOFRestaurant *provisionalRest = [[WOFRestaurant alloc] initWithDictionary:restaurantDic];
        [restsArray addObject:provisionalRest];
    }
    
    [restsArray sortUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [[(WOFObject*)a name] lowercaseString];
        NSString *second = [[(WOFObject*)b name] lowercaseString];
        return [first compare:second];
    }];
    
    return restsArray;
}

- (NSMutableArray *) createFoodsArrayfromArray: (NSArray *) foodJSONs{
    NSMutableArray *foodsArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *foodDic in foodJSONs) {
        WOFFood *provisionalFood = [[WOFFood alloc] initWithDictionary:foodDic];
        [foodsArray addObject:provisionalFood];
    }
    
    [foodsArray sortUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [[(WOFObject*)a name] lowercaseString];
        NSString *second = [[(WOFObject*)b name] lowercaseString];
        return [first compare:second];
    }];
    
    return foodsArray;
}

#pragma mark - Actions

- (IBAction)changedFilter:(id)sender {
    [self.searchBar resignFirstResponder];
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    
    switch (segmentedControl.selectedSegmentIndex) {
        case 0:
            selectedFilter = ALL;
            (restaurantArray.count == 0) ? [self showNotFoundViewWithAnimationWithText:@"No hubo ningún resultado para tu búsqueda"] : [self.notFoundView setHidden:YES];
            break;
        case 1:
            selectedFilter = REST;
            (restaurantArray.count == 0) ? [self showNotFoundViewWithAnimationWithText:@"No se encontraron lugares con tu búsqueda"] : [self.notFoundView setHidden:YES];
            break;
        case 2:
            (foodArray.count == 0) ? [self showNotFoundViewWithAnimationWithText:@"No se encontró comida con tu búsqueda"] : [self.notFoundView setHidden:YES];
            selectedFilter = FOOD;
            break;
        default:
            break;
    }
    
    [self.searchCollectionView reloadData];
}

- (void) showNotFoundViewWithAnimationWithText: (NSString *) viewText{
    notFoundViewText.text = viewText;
    [UIView animateWithDuration:0.4f animations:^ {
        [self.notFoundView setHidden:NO];
    }];
}

#pragma mark - Search Bar delegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [UIView animateWithDuration:0.4f animations:^ {
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar" style:UIBarButtonItemStyleDone target:self action:@selector(cancelSearch)];
    }];
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (![searchText isEqualToString:@""] && searchText.length >= 3) {
        [self performRequestforText:searchText];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

- (void) cancelSearch{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void) dismissKeyboard{
    [self.searchBar resignFirstResponder];
}

#pragma mark - CollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    // Return the number of sections.
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    if (selectedFilter == REST) {
        return restaurantArray.count;
    }else if (selectedFilter == FOOD){
        return foodArray.count;
    }
    return tableDataArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WOFRestaurantCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    WOFObject *provisionalObject;
    
    if (selectedFilter == REST) {
        provisionalObject = [restaurantArray objectAtIndex:indexPath.row];
    }else if (selectedFilter == FOOD){
        provisionalObject = [foodArray objectAtIndex:indexPath.row];
    }else{
        provisionalObject = [tableDataArray objectAtIndex:indexPath.row];
    }
    
    [cell createCellWithObject:provisionalObject];
    
    return cell;
}

#pragma mark CollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.searchBar resignFirstResponder];
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    WOFObject *selectedObject;
    
    if (selectedFilter == REST) {
        selectedObject = [restaurantArray objectAtIndex:indexPath.row];
    }else if (selectedFilter == FOOD){
        selectedObject = [foodArray objectAtIndex:indexPath.row];
    }else{
        selectedObject = [tableDataArray objectAtIndex:indexPath.row];
    }
    
    if (selectedObject.isRestaurant) {
        selectedRestaurant = (WOFRestaurant *) selectedObject;
        [self performSegueWithIdentifier:@"restaurantSearchDisplay" sender:self];
    }else{
        selectedFood = (WOFFood *) selectedObject;
        [self performSegueWithIdentifier:@"foodSearchDetail" sender:self];
    }
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeihgt = cellSide + (cellSide * 0.25f);
    CGSize cell = CGSizeMake(cellSide, cellHeihgt);
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark collection view cell paddings
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 2.5, 5, 2.5); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"restaurantSearchDisplay"]) {
        WOFRestaurantDetailViewController *nextVC = [segue destinationViewController];
        nextVC.detailRestaurant = selectedRestaurant;
    }
    
    if ([segue.identifier isEqualToString:@"foodSearchDetail"]) {
        WOFFoodDetailViewController *nextVC = [segue destinationViewController];
        nextVC.detailFood = selectedFood;
    }
    
    if ([segue.identifier isEqualToString:@"scannMenu"]) {
        WOFScannerViewController *nextVC = [segue destinationViewController];
        nextVC.delegate = self;
    }
}

#pragma mark - Scanner delegate

- (void)restaurantFound:(WOFRestaurant *)foundRestaurant{
    selectedRestaurant = foundRestaurant;
    wasScanned = YES;
    [self performSegueWithIdentifier:@"restaurantSearchDisplay" sender:nil];
}

#pragma mark - Request 

- (void) performRequestforText: (NSString *) searchText{
    [progressHUD show:YES];
    [requestHelper searchString:searchText performSelector:@"populateInfoWithResponse:"];
    networkIdentifier.networkActivityIndicatorVisible = YES;
}

- (void) requestDidFinishRequestingData:(NSDictionary *)responseDictionary performSelector:(NSString *)selector{
    [self performSelector:NSSelectorFromString(selector) withObject:responseDictionary afterDelay:0.0];
}

- (void) requestErrorInConnection{
    [WOFViewHelper presentDefaultAlertViewWithTitle:@"Hubo un error." withMessage:@"Hubo un error al tratar de contactar el servidor. Vuelve a intentarlo más tarde." inViewController:self];
    [self showConnectionPerformedSuccessful:NO];
}

- (void) showConnectionPerformedSuccessful: (BOOL) wasSuccessful{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    [progressHUD hide:YES];
    
    if (wasSuccessful) {
        [self.notFoundView setHidden:YES];
        [self.searchCollectionView reloadData];
    }else{
        [self showNotFoundViewWithAnimationWithText:@"No hubo ningún resultado para tu búsqueda"];
    }
}

@end
