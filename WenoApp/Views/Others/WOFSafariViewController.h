//
//  WOFSafariViewController.h
//  WenoApp
//
//  Created by Luis Fernando Mata on 7/2/16.
//  Copyright © 2016 Weno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WOFSafariViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *safariView;

@end
