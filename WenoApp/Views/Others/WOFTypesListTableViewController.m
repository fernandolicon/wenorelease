//
//  WOFTypesListTableViewController.m
//  WenoApp
//
//  Created by Luis Fernando Mata on 29/6/15.
//  Copyright (c) 2015 Weno. All rights reserved.
//

#import "WOFTypesListTableViewController.h"
#import "WOFRestaurantListCollectionViewController.h"
#import "WOFRequestHelper.h"
#import "Haneke.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "WOFTypeListTableViewCell.h"

@interface WOFTypesListTableViewController ()<WOFRequestHelperDelegate>{
    WOFRequestHelper *requestHelper;
    NSMutableArray *types;
    CGFloat height;
    MBProgressHUD *progressHUD;
    NSDictionary *selectedType;
    BOOL emptyResponse;
    IBOutlet UIView *notFoundView;
}

@end

@implementation WOFTypesListTableViewController

@synthesize selection;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //Set Not Found view size
    CGRect newFrame = notFoundView.frame;
    newFrame.size.width = self.view.frame.size.width;
    newFrame.size.height = newFrame.size.height;
    [notFoundView setFrame:newFrame];
    
    requestHelper = [[WOFRequestHelper alloc] init];
    requestHelper.delegate = self;
    
    progressHUD = [[MBProgressHUD alloc] init];
    [self.view addSubview:progressHUD];
    progressHUD.labelText = @"Cargando";
    
    emptyResponse = NO;
    
    types = [[NSMutableArray alloc] init];
    
    //To get the correct height we need to calculate it using the 7:2 aspect ratio we're using
    CGFloat width = self.view.frame.size.width;
    height = (2.0f / 7.0f) * width;
    
    [self requestData];
}

- (void) viewWillAppear:(BOOL)animated{
    self.title = [selection objectForKey:@"shortName"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Data for table

- (void) populateDictionarieswithResponse: (NSDictionary *) responseObject{
    for (NSDictionary *jsonDictionary in responseObject) {
        [types addObject:jsonDictionary];
    }
    
    emptyResponse = (types.count == 0) ? YES : NO;
    
    [self showConnectionPerformedSuccessful:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (emptyResponse) {
        return 1;
    }
    return types.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (emptyResponse) {
        WOFTypeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"typeListCell" forIndexPath:indexPath];
        cell.textLabel.text =  @"No se encontró información";
        
        return cell;
    }
    
    WOFTypeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"typeListCell" forIndexPath:indexPath];
    
    if ((indexPath.row % 2) != 0) {
        [cell setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:67.0f/255.0f blue:1.0f/255.0f alpha:1]];
    }else{
        [cell setBackgroundColor:[UIColor colorWithRed:102.0f/255.0f green:102.0f/255.0f blue:102.0f/255.0f alpha:1]];
    }
    
    NSDictionary *provisionalDictionary = [types objectAtIndex:indexPath.row];
    
    cell.typeListText.text = [NSString stringWithFormat:@"%@ \n%@", [provisionalDictionary objectForKey:@"name"], [provisionalDictionary objectForKey:@"description"]];
    //NSURL *imageURL = [NSURL URLWithString:[provisionalDictionary objectForKey:@"photo"]];
    //[cell.backgroundImage hnk_setImageFromURL:imageURL];

    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    selectedType = [types objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"showCollectionRestaurant" sender:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showCollectionRestaurant"]) {
        WOFRestaurantListCollectionViewController *nextVC = [segue destinationViewController];
        nextVC.typeSelection = [selectedType objectForKey:@"name"];
        nextVC.selectionRoute = [selection objectForKey:@"typeRoute"];
    }
}

#pragma mark - Request

- (void) requestData{
    [progressHUD show:YES];
    [requestHelper restaurantType:[selection objectForKey:@"route"] performSelector:@"populateDictionarieswithResponse:"];
}

- (void) requestDidFinishRequestingData:(NSDictionary *)responseDictionary performSelector:(NSString *)selector{
    [self performSelector:NSSelectorFromString(selector) withObject:responseDictionary afterDelay:0.0];
}

- (void) requestErrorInConnection{
    [self showConnectionPerformedSuccessful:NO];
}

- (void) showConnectionPerformedSuccessful: (BOOL) wasSuccessful{
    [progressHUD hide:YES];
    
    if (wasSuccessful) {
        [self.tableView reloadData];
    }else{
        [self.view addSubview:notFoundView];
    }
}


- (IBAction)tryAgain:(id)sender {
    [notFoundView removeFromSuperview];
    [self requestData];
}
@end
